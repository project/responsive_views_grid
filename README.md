CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The module will provide a responsive grid view formatter to display items in a
responsive grid.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/responsive_views_grid

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/responsive_views_grid


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Responsive Views Grid module as you would normally install a
   contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

When creating a view, there is now an option to choose a responsive grid format.


MAINTAINERS
-----------

 * Malabya Tewari (imalabya) - https://www.drupal.org/u/imalabya

Supporting organizations:

 * Specbee Consulting Services Pvt. Ltd - https://www.drupal.org/specbee-consulting-services-pvt-ltd
